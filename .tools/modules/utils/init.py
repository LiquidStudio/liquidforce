from colorama import Fore, Style
import subprocess


def call_subprocess(command, verbose=True):
    ''' Calls subprocess, returns output and return code,
        if verbose flag is active it will print the output '''
    try:
        stdout = subprocess.check_output(command, stderr=subprocess.STDOUT,
                                         shell=True).decode('utf-8')
        if verbose:
            print_output(f'{Style.DIM}{stdout}{Style.NORMAL}')
        return stdout, 0
    except subprocess.CalledProcessError as exc:
        output = exc.output.decode('utf-8')
        returncode = exc.returncode
        if verbose:
            print(f'Subprocess returned non-zero exit '
                  f'status {returncode}')
            print_output(output, color=Fore.RED)
        return output, returncode


def print_output(output, color='', tab_level=1):
    ''' Prints output in the color passed '''
    formated = '\t' * tab_level + output.replace('\n', '\n' + '\t' * tab_level)
    print(f'{color}{formated}{Fore.RESET}')

